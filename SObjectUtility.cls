public with sharing class SObjectUtility
{  
  

    public static void SubmitRecordsToApprove(List<SObject> records, String approvalProcessName, String comments, String submitterFieldId)
    {
        
        //List<Approval.ProcessSubmitRequest> appProcList = new List<Approval.ProcessSubmitRequest>();
        for (SObject r : records)
        {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(comments);
            req1.setObjectId(r.Id);
            req1.setProcessDefinitionNameOrId(approvalProcessName);
            
            
            System.debug('+++++++++++++++++++++++++++++++submitterFieldId'+submitterFieldId);
            
            Id SubmitterId = r.get(submitterFieldId+'')+'';
            System.debug('+++++++++++++++++++++++++++++++SubmitterId'+SubmitterId);

            
            req1.setSubmitterId(SubmitterId); 

            //appProcList.add(New Approval.ProcessSubmitRequest());
            Approval.ProcessResult results = Approval.process(req1);
        
        }

    } 
    
    
    public static string getTheQueryAllFields(String objectName, String otherFields)
    {

        otherFields = otherFields==null ? '' : otherFields.deleteWhitespace();
    
        // Initialize setup variables
        String query = 'SELECT ';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
    
        // Grab the fields from the describe method and append them to the queryString one by one.
        for(String s : objectFields.keySet()) {
           query += '' + s + ',';
        }
    
        // Manually add related object's fields that are needed.
        //query += 'Account.Name,'; // modify as needed
        String additionalFields = (otherFields == null ? '' : otherFields);
        
        query += otherFields;
    
        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }
    
        // Add FROM statement
        query += ' FROM ' + objectName +' ';
    
        // Add on a WHERE/ORDER/LIMIT statement as needed
        //query += ' WHERE firstName = \'test\''; // modify as needed
        
        //system.debug('SObjectUtility.getTheQueryAllFields('+objectName+','+otherFields+') ------->'+query);
        system.debug('SObjectUtility.getTheQueryAllFields(\''+objectName+'\',\''+otherFields+'\'); ------->'+query);
        return query;

    }
    





}